package be.dastudios.teamkoala.legendofthelamb.models;

public class Thing implements java.io.Serializable {
// Basic Thing type that defines all objects in the Adventure

    private String name;
    private String description;
    private boolean takable;
    private boolean movable;

    private ThingHolder container;

    //region Constructors

    public Thing(String aName, String aDescription, ThingHolder aContainer) {
        // constructor
        this.name = aName;
        this.description = aDescription;
        this.takable = true;
        this.movable = true;
        this.container = aContainer;
    }

    public Thing(String aName, String aDescription, boolean canTake, boolean canMove, ThingHolder aContainer) {
        // constructor
        this.name = aName;
        this.description = aDescription;
        this.takable = canTake;
        this.movable = canMove;
        this.container = aContainer;
    }
    //endregion

    //region Getters and setters

    public String getName() {

        return name;
    }

    public void setName(String aName) {

        this.name = aName;
    }

    public String getDescription() {

        return description;
    }

    public void setDescription(String aDescription) {

        this.description = aDescription;
    }

    public boolean isTakable() {

        return takable;
    }

    public void setTakable(boolean takable) {

        this.takable = takable;
    }

    public boolean isMovable() {

        return movable;
    }

    public void setMovable(boolean movable) {

        this.movable = movable;
    }

    public ThingHolder getContainer() {

        return container;
    }

    public void setContainer(ThingHolder container) {

        this.container = container;
    }

    //endregion

    //region Open - close

    public String open() {

        return "Cannot open " + name + " because it isn't a container.";
    }

    public String close() {

        return "Cannot close " + name + " because it isn't a container.";
    }
    //endregion

    public String describe() {

        return name + " " + description;
    }

    //region Inside methods

    private boolean isInside(ContainerThing aContainer) {

        ThingHolder thingHolder;
        Boolean isInContainer = false;

        //  Check if this thing has a container
        thingHolder = this.getContainer();

        //  If this thing has a container, check if it is aContainer
        while (thingHolder != null) {
            if (thingHolder == aContainer) {
                isInContainer = true;//   This thing is in aContainer
            }
            thingHolder = thingHolder.getContainer();// This thing is in a container, but not in aContainer
        }
        return isInContainer;
    }

    public boolean isIn(Thing t) {

        return (t instanceof ContainerThing) && (this.isInside((ContainerThing) t));
    }
    //endregion


    @Override
    public String toString() {

        return "Thing{" +

                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", takable=" + takable +
                ", movable=" + movable +
                ", container=" + container +
                '}';
    }
}