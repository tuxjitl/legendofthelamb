package be.dastudios.teamkoala.legendofthelamb;

import be.dastudios.teamkoala.legendofthelamb.game.AdventureGame;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {


        try {
            AdventureGame.main(null);
        }
        catch (IOException e) {
            e.printStackTrace();
        }


    }
}