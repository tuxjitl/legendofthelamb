package be.dastudios.teamkoala.legendofthelamb.globals;

public enum GrammarType {
    NOUN,//the name of a specific object e.g. cat, dog, etc.
    VERB,//conveys an action
    ADJECTIVE,//modifies a noun or verb or describes it's reference e.g. the, this, my, etc.
    ARTICLE,//like a, the, an
    PREPOSITION,//in combination with noun phrases, e.g. in, under, of
    UNKNOWN,//Not known in our vocabulary map
    ERROR;

}
