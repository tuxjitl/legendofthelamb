package be.dastudios.teamkoala.legendofthelamb.utilities.fileutility;

import be.dastudios.teamkoala.legendofthelamb.game.Game;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class FileUtility {

    private static final String INVALID_MSG = "Invalid input! Please try again...";
    public static final Scanner KEYBOARD = new Scanner(System.in).useLocale(Locale.ENGLISH);

    public static String[] getItemsFromFile(Path p) {

        Path path = p;

        try (
                Stream<String> dataLines = Files.newBufferedReader(path).lines();//Java 8


        ) {

            return dataLines.toArray(String[]::new);
        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return null;

    }

    public static boolean checkExistsPath(Path aPath) {

        if (Files.exists(aPath)) {
            return true;
        }
        return false;
    }

    /**
     * Static method  for asking a choice input: y/n.
     *
     * @param message Show the prompt passed as argument: e.g. Do you wish to continue.
     *
     * @return boolean Returns the input from the user: e.g. y.
     */
    public static boolean askYOrN(String message) {

        while (true) {
            String input = ask(message + " (y/n)");
            char firstLetter = '0';
            try {
                firstLetter = input.toLowerCase().charAt(0);

                switch (firstLetter) {
                    case 'y':
                        return true;
                    case 'n':
                        return false;
                    default:
                        throw new Exception();
                }
            }
            catch (Exception e) {
                System.out.println(INVALID_MSG);
            }
        }
    }

    /**
     * Static method for asking a choice.
     *
     * @param message Show the prompt passed as argument: e.g. Choose a date.
     * @param options A String array of possible choices.
     *
     * @return int The index of the item selected in the array.
     */
    public static int askForChoice(String message, String[] options) {

        System.out.println(message);
        return askForChoice(options);
    }

    /**
     * Static method invoked by askForChoice method with message.
     *
     * @param options String array of possible choices.
     *
     * @return int The index of the item selected in the array.
     */
    public static int askForChoice(String[] options) {

        while (true) {
            for (int i = 0; i < options.length; i++) {
                System.out.printf("%d. %s%n", i + 1, options[i]);
            }
            int chosenIdx = askForInt(String.format("Enter your choice (1-%d)", options.length)) - 1;
            if (chosenIdx < 0 || chosenIdx >= options.length) {
                System.out.println(INVALID_MSG);
                System.out.println("Please enter a choice in the valid range");
                System.out.println();
            }
            else {
                return chosenIdx;
            }
        }
    }

    /**
     * Static method  for asking a string as input e.g. a name.
     *
     * @param message Show the prompt passed as argument: e.g. Enter your name.
     *
     * @return String Returns the input from the user: e.g. Malcom
     */
    public static String ask(String message) {

        while (true) {
            System.out.print(message + ": ");
            String input = KEYBOARD.nextLine().trim();
            try {
                if (input.length() > 0) {

                    return input;
                }
                else {
                    throw new InputMismatchException(INVALID_MSG);
                }
            }
            catch (InputMismatchException ime) {
                System.out.println(ime.getMessage());
            }
            catch (Exception e) {
                System.out.println(INVALID_MSG);
            }
        }
    }

    /**
     * Static method  for asking an int input.
     *
     * @param message Show the prompt passed as argument: e.g. Enter the number of persons.
     *
     * @return int Returns the input from the user: e.g. 5.
     */
    public static int askForInt(String message) {

        while (true) {
            String input = ask(message);
            try {
                return Integer.parseInt(input);
            }
            catch (Exception e) {
                System.out.println(INVALID_MSG);
            }
        }
    }

    public static String[] getNamesFromEnum(Class<? extends Enum<?>> e) {

        return Arrays.stream(e.getEnumConstants()).map(Enum::name).toArray(String[]::new);
    }

    public static String sanitizeString(String input) {

        String delims = "[ \t,.:;?!\"']+";
        List<String> strlist = new ArrayList<>();
        String[] words = input.split(delims);

        for (String word : words) {

            String temp = word.replaceAll("-", "");
            strlist.add(temp);
        }

        String strInput = "";
        for (String word : strlist) {
            strInput += word;
        }

        strInput.replace("-", "");

        return strInput;
    }

    public static boolean overwriteFile(String fn) {

        boolean ok;
        String s;

        ok = FileUtility.askYOrN(fn + " exists. Overwrite?");

        return ok;
    }

    public static boolean fileExists(String fn) {

        boolean exists;
        File f;

        f = new File(fn);
        exists = f.exists();
        return exists;
    }

    public static String getFileName(String name, String FILE_EXT) {

        String ext;

        String fn = FileUtility.ask("Enter a file name: ");

        if (!fn.isEmpty()) {
            ext = FileUtility.ask("Enter an extension");
            if (!ext.equals(FILE_EXT)) {
                System.out.println("Error: File must have extension: " + FILE_EXT);
                fn = "";
            }
        }
        return fn;
    }

    public static String makeFileName(String name) {

        String localTimeStamp = LocalDateTime.now().toString();
        String tempFn = name + localTimeStamp;
        String fn = FileUtility.sanitizeString(tempFn);
        return fn;
    }

    public static void saveToDestination(Path newFolder, String fileName, String FILE_EXT, Game game) throws IOException {

        Path savedGame = Path.of(newFolder.toString()).resolve(fileName + FILE_EXT);
        FileOutputStream fos = new FileOutputStream(String.valueOf(savedGame));
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(game); // game
        oos.flush(); // write out any buffered bytes
        oos.close();
        System.out.print("Game saved\n");
    }

    public static boolean deletefilesDirectory(Path pathToPlayerDirectory, String FILE_EXT) {

        File[] list = null;
        list = getFileList(pathToPlayerDirectory, FILE_EXT);

        try {
            if (list.length == 0) {
                System.out.println("There are no files to delete.");
                System.out.println();
                return false;
            }
            else {
                Files.newDirectoryStream(pathToPlayerDirectory).forEach(file -> {
                    try {
                        Files.delete(file);
                    }
                    catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            }

        }
        catch (IOException e) {
            e.printStackTrace();
        }

        return true;
    }

    public static String[] convertFilesListInStringList(File[] files) {

        String separator = "\\";
        String[] strFiles = null;

        strFiles = new String[files.length];
        for (int i = 0; i < files.length; i++) {
            String temp = files[i].toString();
            String[] arrvalues = temp.split(Pattern.quote(separator));
            String fileName = arrvalues[arrvalues.length - 1];
            strFiles[i] = fileName.substring(0, fileName.length() - 4);
        }

        return strFiles;

    }

    public static File[] getFileList(Path pathToPlayerMap, String FILE_EXT) {

        File[] files = null;

        try {
            File f = new File(pathToPlayerMap.toString());

            FilenameFilter filter = new FilenameFilter() {
                @Override
                public boolean accept(File f, String name) {

                    return name.endsWith(FILE_EXT);
                }
            };

            files = f.listFiles(filter);
        }
        catch (Exception e) {
            System.err.println(e.getMessage());
        }
        return files;
    }

}