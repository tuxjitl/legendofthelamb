package be.dastudios.teamkoala.legendofthelamb.menu;

import java.nio.file.Path;

public class DisplayMenu {

    public static void displayMenuItem(Path path) {
        CustomFileReader.readTextFile(Path.of(String.valueOf(path)));
    }
}